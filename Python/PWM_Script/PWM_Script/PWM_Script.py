
import socket
import threading
from threading import Thread
import os
import sys
import struct
import warnings
import time
#import RPi.GPIO as GPIO

#GPIO.setmode(GPIO.BOARD)
#GPIO.setup(12, GPIO.OUT)


def socket_service():                                                            #SERVERTEIL
   try:
       s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
       s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
       s.bind(('127.0.0.1', 1234))
       s.listen(10)
   except socket.error as msg:
       print(msg)
       sys.exit(1)

   print("[*]Wait for Connection.....................")

   while True:
       sock, addr = s.accept()
       print("[*] Accepted connection from: %s:%d" % (addr[0],addr[1]))
       com=threading.Thread(target=com_client,args=(sock,addr))
       com.start()


def com_client(sock,addr):                                                        #PARSER
       com_request = sock.recv(1024)
       data = com_request.decode()
       print(data)
       if data==":remote:PWM1000":
           print(data)
           Start_PWM_1000()

       elif data==":remote:PWM670":
           print(data)
           Start_PWM_670()

       elif data==":remote:PWM2010":
           print(data)
           Start_PWM_2010()

def Start_PWM_1000():                                                               #FUNKTIONEN
        #pwm = GPIO.PWM(12, 1000)
        print("Beginne Senden")
        #pwm.start(50)
        time.sleep(10)
        print("Senden beendet")
        #pwm.stop()
        #GPIO.cleanup()
        
def Start_PWM_670():
        #pwm = GPIO.PWM(12, 670)
        print("Beginne Senden")
        #pwm.start(50)
        time.sleep(10)
        print("Senden beendet")
        #pwm.stop()
        #GPIO.cleanup()   

def Start_PWM_2010():
        #pwm = GPIO.PWM(12, 2010)
        print("Beginne Senden")
        #pwm.start(50)
        time.sleep(10)
        print("Senden beendet")
        #pwm.stop()
        #GPIO.cleanup()   

if __name__=='__main__':
       socket_service()
       #Thread(target=socket_service).start()