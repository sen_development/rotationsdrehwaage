﻿<?xml version='1.0' encoding='UTF-8'?>
<Project Type="Project" LVVersion="17008000">
	<Property Name="SMProvider.SMVersion" Type="Int">201310</Property>
	<Item Name="My Computer" Type="My Computer">
		<Property Name="IOScan.Faults" Type="Str"></Property>
		<Property Name="IOScan.NetVarPeriod" Type="UInt">100</Property>
		<Property Name="IOScan.NetWatchdogEnabled" Type="Bool">false</Property>
		<Property Name="IOScan.Period" Type="UInt">10000</Property>
		<Property Name="IOScan.PowerupMode" Type="UInt">0</Property>
		<Property Name="IOScan.Priority" Type="UInt">9</Property>
		<Property Name="IOScan.ReportModeConflict" Type="Bool">true</Property>
		<Property Name="IOScan.StartEngineOnDeploy" Type="Bool">false</Property>
		<Property Name="NI.SortType" Type="Int">3</Property>
		<Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="server.tcp.enabled" Type="Bool">false</Property>
		<Property Name="server.tcp.port" Type="Int">0</Property>
		<Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
		<Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
		<Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
		<Property Name="specify.custom.address" Type="Bool">false</Property>
		<Item Name="Actors" Type="Folder">
			<Item Name="Functions" Type="Folder">
				<Item Name="Coil.lvlib" Type="Library" URL="../Coil/Coil.lvlib"/>
				<Item Name="Power.lvlib" Type="Library" URL="../Stromzufur/Stromzufur/Power.lvlib"/>
				<Item Name="RotationMotor.lvlib" Type="Library" URL="../Rotationsmotor/RotationMotor.lvlib"/>
				<Item Name="Stopper.lvlib" Type="Library" URL="../Stopper/Stopper.lvlib"/>
				<Item Name="Stopwatch.lvlib" Type="Library" URL="../Stopwatch/Stopwatch.lvlib"/>
				<Item Name="Weights.lvlib" Type="Library" URL="../Weights/Weights.lvlib"/>
				<Item Name="LightBarrier.lvlib" Type="Library" URL="../LightBarrier/LightBarrier.lvlib"/>
				<Item Name="Timer5Min.lvlib" Type="Library" URL="../Timer5Min/Timer5Min.lvlib"/>
				<Item Name="Conf.lvlib" Type="Library" URL="../Conf/Conf.lvlib"/>
			</Item>
			<Item Name="SubGUI" Type="Folder">
				<Item Name="StudentGui.lvlib" Type="Library" URL="../GUI_Schüler/GUI_Schüler/StudentGui.lvlib"/>
				<Item Name="TeacherGui.lvlib" Type="Library" URL="../GUI_Lehrer/GUI_Lehrer/TeacherGui.lvlib"/>
			</Item>
			<Item Name="Structure" Type="Folder">
				<Item Name="Base.lvlib" Type="Library" URL="../Base/Base.lvlib"/>
				<Item Name="Controller.lvlib" Type="Library" URL="../Controller/Controller.lvlib"/>
				<Item Name="ContainerManager.lvlib" Type="Library" URL="../ContainerManager/ContainerManager.lvlib"/>
			</Item>
		</Item>
		<Item Name="Config" Type="Folder" URL="../Config">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="SubVis" Type="Folder" URL="../SubVis">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Treiber" Type="Folder" URL="../Treiber">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="TypeDef" Type="Folder" URL="../TypeDef">
			<Property Name="NI.DISK" Type="Bool">true</Property>
		</Item>
		<Item Name="Launch.vi" Type="VI" URL="../Launch.vi"/>
		<Item Name="Main.vi" Type="VI" URL="../Main.vi"/>
		<Item Name="Dependencies" Type="Dependencies">
			<Item Name="vi.lib" Type="Folder">
				<Item Name="8.6CompatibleGlobalVar.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/config.llb/8.6CompatibleGlobalVar.vi"/>
				<Item Name="Actor Framework.lvlib" Type="Library" URL="/&lt;vilib&gt;/ActorFramework/Actor Framework.lvlib"/>
				<Item Name="Application Directory.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Application Directory.vi"/>
				<Item Name="BuildHelpPath.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/BuildHelpPath.vi"/>
				<Item Name="Check if File or Folder Exists.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/libraryn.llb/Check if File or Folder Exists.vi"/>
				<Item Name="Check Special Tags.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Check Special Tags.vi"/>
				<Item Name="Clear Errors.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Clear Errors.vi"/>
				<Item Name="compatOverwrite.vi" Type="VI" URL="/&lt;vilib&gt;/_oldvers/_oldvers.llb/compatOverwrite.vi"/>
				<Item Name="Convert property node font to graphics font.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Convert property node font to graphics font.vi"/>
				<Item Name="Details Display Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Details Display Dialog.vi"/>
				<Item Name="DialogType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogType.ctl"/>
				<Item Name="DialogTypeEnum.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/DialogTypeEnum.ctl"/>
				<Item Name="Error Cluster From Error Code.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Cluster From Error Code.vi"/>
				<Item Name="Error Code Database.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Error Code Database.vi"/>
				<Item Name="ErrWarn.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/ErrWarn.ctl"/>
				<Item Name="eventvkey.ctl" Type="VI" URL="/&lt;vilib&gt;/event_ctls.llb/eventvkey.ctl"/>
				<Item Name="ex_CorrectErrorChain.vi" Type="VI" URL="/&lt;vilib&gt;/express/express shared/ex_CorrectErrorChain.vi"/>
				<Item Name="Find Tag.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Find Tag.vi"/>
				<Item Name="Format Message String.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Format Message String.vi"/>
				<Item Name="General Error Handler Core CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler Core CORE.vi"/>
				<Item Name="General Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/General Error Handler.vi"/>
				<Item Name="Get LV Class Name.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/LVClass/Get LV Class Name.vi"/>
				<Item Name="Get String Text Bounds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Get String Text Bounds.vi"/>
				<Item Name="Get Text Rect.vi" Type="VI" URL="/&lt;vilib&gt;/picture/picture.llb/Get Text Rect.vi"/>
				<Item Name="GetHelpDir.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetHelpDir.vi"/>
				<Item Name="GetRTHostConnectedProp.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/GetRTHostConnectedProp.vi"/>
				<Item Name="High Resolution Relative Seconds.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/High Resolution Relative Seconds.vi"/>
				<Item Name="Internecine Avoider.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/Internecine Avoider.vi"/>
				<Item Name="Longest Line Length in Pixels.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Longest Line Length in Pixels.vi"/>
				<Item Name="LVBoundsTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVBoundsTypeDef.ctl"/>
				<Item Name="LVRectTypeDef.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/miscctls.llb/LVRectTypeDef.ctl"/>
				<Item Name="NI_FileType.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/lvfile.llb/NI_FileType.lvlib"/>
				<Item Name="NI_LVConfig.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/config.llb/NI_LVConfig.lvlib"/>
				<Item Name="NI_PackedLibraryUtility.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/LVLibp/NI_PackedLibraryUtility.lvlib"/>
				<Item Name="NI_SystemLogging.lvlib" Type="Library" URL="/&lt;vilib&gt;/Utility/SystemLogging/NI_SystemLogging.lvlib"/>
				<Item Name="Not Found Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Not Found Dialog.vi"/>
				<Item Name="Search and Replace Pattern.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Search and Replace Pattern.vi"/>
				<Item Name="Set Bold Text.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set Bold Text.vi"/>
				<Item Name="Set String Value.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Set String Value.vi"/>
				<Item Name="Simple Error Handler.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Simple Error Handler.vi"/>
				<Item Name="Space Constant.vi" Type="VI" URL="/&lt;vilib&gt;/dlg_ctls.llb/Space Constant.vi"/>
				<Item Name="subDisplayMessage.vi" Type="VI" URL="/&lt;vilib&gt;/express/express output/DisplayMessageBlock.llb/subDisplayMessage.vi"/>
				<Item Name="subTimeDelay.vi" Type="VI" URL="/&lt;vilib&gt;/express/express execution control/TimeDelayBlock.llb/subTimeDelay.vi"/>
				<Item Name="System Exec.vi" Type="VI" URL="/&lt;vilib&gt;/Platform/system.llb/System Exec.vi"/>
				<Item Name="TagReturnType.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/TagReturnType.ctl"/>
				<Item Name="TCP Listen Internal List.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen Internal List.vi"/>
				<Item Name="TCP Listen List Operations.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen List Operations.ctl"/>
				<Item Name="TCP Listen.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/TCP Listen.vi"/>
				<Item Name="Three Button Dialog CORE.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog CORE.vi"/>
				<Item Name="Three Button Dialog.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Three Button Dialog.vi"/>
				<Item Name="Time-Delay Override Options.ctl" Type="VI" URL="/&lt;vilib&gt;/ActorFramework/Time-Delayed Send Message/Time-Delay Override Options.ctl"/>
				<Item Name="Trim Whitespace.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/Trim Whitespace.vi"/>
				<Item Name="UDP Multicast Open.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/UDP Multicast Open.vi"/>
				<Item Name="UDP Multicast Read-Only Open.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/UDP Multicast Read-Only Open.vi"/>
				<Item Name="UDP Multicast Read-Write Open.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/UDP Multicast Read-Write Open.vi"/>
				<Item Name="UDP Multicast Write-Only Open.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/tcp.llb/UDP Multicast Write-Only Open.vi"/>
				<Item Name="VISA Configure Serial Port" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port"/>
				<Item Name="VISA Configure Serial Port (Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Instr).vi"/>
				<Item Name="VISA Configure Serial Port (Serial Instr).vi" Type="VI" URL="/&lt;vilib&gt;/Instr/_visa.llb/VISA Configure Serial Port (Serial Instr).vi"/>
				<Item Name="whitespace.ctl" Type="VI" URL="/&lt;vilib&gt;/Utility/error.llb/whitespace.ctl"/>
				<Item Name="Open/Create/Replace File.vi" Type="VI" URL="/&lt;vilib&gt;/Utility/file.llb/Open/Create/Replace File.vi"/>
			</Item>
			<Item Name="AF Debug.lvlib" Type="Library" URL="/&lt;resource&gt;/AFDebug/AF Debug.lvlib"/>
			<Item Name="LV Config Read String.vi" Type="VI" URL="/&lt;resource&gt;/dialog/lvconfig.llb/LV Config Read String.vi"/>
			<Item Name="systemLogging.dll" Type="Document" URL="systemLogging.dll">
				<Property Name="NI.PreserveRelativePath" Type="Bool">true</Property>
			</Item>
		</Item>
		<Item Name="Build Specifications" Type="Build">
			<Item Name="Anzeige Steuerung" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{956D84E9-792A-4E11-BEDE-B16176CE21A4}</Property>
				<Property Name="App_INI_GUID" Type="Str">{D1D7EBB4-2403-4F5C-81C6-EBEE8BC53DE6}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{BC901081-F27A-4A1B-B6D4-FB20FA046818}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">Anzeige Steuerung</Property>
				<Property Name="Bld_defaultLanguage" Type="Str">German</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/builds/NI_AB_PROJECTNAME/Anzeige Steuerung</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{4BC6F6E2-9E35-4242-832D-7439949C72E9}</Property>
				<Property Name="Bld_version.build" Type="Int">9</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Anzeige.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/Anzeige Steuerung/Anzeige.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Hilfsdatei-Verzeichnis</Property>
				<Property Name="Destination[1].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/Anzeige Steuerung/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">Config</Property>
				<Property Name="Destination[2].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/Anzeige Steuerung/Config</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[3].destName" Type="Str">TypeDef</Property>
				<Property Name="Destination[3].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/Anzeige Steuerung/TypeDef</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[4].destName" Type="Str">SubVis</Property>
				<Property Name="Destination[4].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/Anzeige Steuerung/SubVis</Property>
				<Property Name="Destination[4].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">5</Property>
				<Property Name="Source[0].itemID" Type="Str">{8C0022A0-9F74-4ADD-B5E9-2ECEE57E0826}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref"></Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">4</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/SubVis</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/Config</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[4].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/TypeDef</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">5</Property>
				<Property Name="TgtF_companyName" Type="Str">SEN - System Entwicklung Nordhausen GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">Anzeige Steuerung</Property>
				<Property Name="TgtF_internalName" Type="Str">Anzeige Steuerung</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 SEN - System Entwicklung Nordhausen GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">Anzeige Steuerung</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{A3774CD5-F78C-45D6-A4D3-6B28558C0AE0}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Anzeige.exe</Property>
			</Item>
			<Item Name="PC Steuerung" Type="EXE">
				<Property Name="App_copyErrors" Type="Bool">true</Property>
				<Property Name="App_INI_aliasGUID" Type="Str">{05601991-F7E9-4CA5-827E-A93EE2742178}</Property>
				<Property Name="App_INI_GUID" Type="Str">{930C08F2-E13F-4DCD-8FDB-96F1BE4101E8}</Property>
				<Property Name="App_serverConfig.httpPort" Type="Int">8002</Property>
				<Property Name="Bld_autoIncrement" Type="Bool">true</Property>
				<Property Name="Bld_buildCacheID" Type="Str">{F5A8C8FA-3930-4935-B200-A7277B6F3FF9}</Property>
				<Property Name="Bld_buildSpecName" Type="Str">PC Steuerung</Property>
				<Property Name="Bld_defaultLanguage" Type="Str">German</Property>
				<Property Name="Bld_excludeInlineSubVIs" Type="Bool">true</Property>
				<Property Name="Bld_excludeLibraryItems" Type="Bool">true</Property>
				<Property Name="Bld_excludePolymorphicVIs" Type="Bool">true</Property>
				<Property Name="Bld_localDestDir" Type="Path">/C/builds/NI_AB_PROJECTNAME/PC Steuerung</Property>
				<Property Name="Bld_modifyLibraryFile" Type="Bool">true</Property>
				<Property Name="Bld_previewCacheID" Type="Str">{CFBA5360-5EA2-437F-962D-36B08D99C8ED}</Property>
				<Property Name="Bld_userLogFile" Type="Path">/C/builds/Raspberry Pi 2 B/PC Steuerung/Raspberry Pi 2 B_PC Steuerung_log.txt</Property>
				<Property Name="Bld_version.build" Type="Int">3</Property>
				<Property Name="Bld_version.major" Type="Int">1</Property>
				<Property Name="Destination[0].destName" Type="Str">Main.exe</Property>
				<Property Name="Destination[0].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/PC Steuerung/Main.exe</Property>
				<Property Name="Destination[0].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[0].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[0].type" Type="Str">App</Property>
				<Property Name="Destination[1].destName" Type="Str">Hilfsdatei-Verzeichnis</Property>
				<Property Name="Destination[1].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/PC Steuerung/data</Property>
				<Property Name="Destination[1].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].destName" Type="Str">Config</Property>
				<Property Name="Destination[2].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/PC Steuerung/Config</Property>
				<Property Name="Destination[2].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[2].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[3].destName" Type="Str">SubVis</Property>
				<Property Name="Destination[3].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/PC Steuerung/SubVis</Property>
				<Property Name="Destination[3].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[4].destName" Type="Str">TypeDef</Property>
				<Property Name="Destination[4].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/PC Steuerung/TypeDef</Property>
				<Property Name="Destination[4].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="Destination[4].preserveHierarchy" Type="Bool">true</Property>
				<Property Name="Destination[5].destName" Type="Str">Treiber</Property>
				<Property Name="Destination[5].path" Type="Path">/C/builds/NI_AB_PROJECTNAME/PC Steuerung/Treiber</Property>
				<Property Name="Destination[5].path.type" Type="Str">&lt;none&gt;</Property>
				<Property Name="DestinationCount" Type="Int">6</Property>
				<Property Name="Exe_actXinfo_enumCLSID[0]" Type="Str">{1C7DC493-2894-40D4-B2D8-9050637F864E}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[1]" Type="Str">{07D6738E-B91E-44E1-8D62-3BE86BC6F9F3}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[10]" Type="Str">{A433E9DA-DA50-4330-86F4-8278BABBFD93}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[11]" Type="Str">{8C30517F-0552-4A86-8BAF-25E8D9495FC9}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[12]" Type="Str">{16D568C1-629A-4665-AF56-3F71E325CF5B}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[13]" Type="Str">{9D5FF18E-8131-4BAA-91AC-F95FBF56C89A}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[14]" Type="Str">{18DD10B0-EEE1-4928-A85D-891FF1C35342}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[15]" Type="Str">{1F232252-4A20-4A66-A1C7-3366E85E2A57}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[16]" Type="Str">{FE78DD3A-D1BA-4F8A-8A00-F0AF681269DB}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[2]" Type="Str">{963D0055-2A69-4ECC-8918-0F1BEC09533C}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[3]" Type="Str">{F0EBF374-8D20-42FD-9B78-790B03B3BCCE}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[4]" Type="Str">{DF4D62EF-DB75-4617-A4CC-5CE3AC8AF315}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[5]" Type="Str">{2465234E-A59E-47B3-8C14-9F7D7B13D1A7}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[6]" Type="Str">{0E663A59-4284-4EF7-BD24-C4C83863FAC4}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[7]" Type="Str">{1DFADAA2-DB06-49B8-AE45-ADA3A57A7013}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[8]" Type="Str">{68AE961D-8C0C-4D61-B446-D6E54CCCFA6D}</Property>
				<Property Name="Exe_actXinfo_enumCLSID[9]" Type="Str">{3F0F481A-3B14-49AC-860F-0F36B5285CEB}</Property>
				<Property Name="Exe_actXinfo_enumCLSIDsCount" Type="Int">17</Property>
				<Property Name="Exe_actXinfo_majorVersion" Type="Int">5</Property>
				<Property Name="Exe_actXinfo_minorVersion" Type="Int">5</Property>
				<Property Name="Exe_actXinfo_objCLSID[0]" Type="Str">{3574A785-2F0B-4090-AD47-8FB1C7302DE9}</Property>
				<Property Name="Exe_actXinfo_objCLSID[1]" Type="Str">{CD2427DF-CACE-49DB-8670-E7F40F4B4A73}</Property>
				<Property Name="Exe_actXinfo_objCLSID[10]" Type="Str">{C86786B9-98DD-41B0-86D5-A74F3080A9CB}</Property>
				<Property Name="Exe_actXinfo_objCLSID[11]" Type="Str">{55D45219-B53D-41DB-988A-DB668B2B2FFA}</Property>
				<Property Name="Exe_actXinfo_objCLSID[12]" Type="Str">{FC40DC3F-3273-4B2E-B608-D433E5650F24}</Property>
				<Property Name="Exe_actXinfo_objCLSID[13]" Type="Str">{33C7DF02-30CE-4514-A91B-4B9F7AE41CC9}</Property>
				<Property Name="Exe_actXinfo_objCLSID[2]" Type="Str">{BE8FAB86-796D-4663-A1F1-EF1A2E261272}</Property>
				<Property Name="Exe_actXinfo_objCLSID[3]" Type="Str">{D35B2ACD-A945-4B74-A816-DB1C28A1AB83}</Property>
				<Property Name="Exe_actXinfo_objCLSID[4]" Type="Str">{7712B36B-5D88-4F00-A51C-7D4CFF55A571}</Property>
				<Property Name="Exe_actXinfo_objCLSID[5]" Type="Str">{5F790043-B825-487C-A340-EC0FF3FCE11D}</Property>
				<Property Name="Exe_actXinfo_objCLSID[6]" Type="Str">{2BED89D6-CB06-4B1F-B40B-462648DB9A00}</Property>
				<Property Name="Exe_actXinfo_objCLSID[7]" Type="Str">{F6BD9E99-72A3-4F85-B30C-84FDA348154E}</Property>
				<Property Name="Exe_actXinfo_objCLSID[8]" Type="Str">{B18EE48E-28C6-42C9-B129-5546E2795C6B}</Property>
				<Property Name="Exe_actXinfo_objCLSID[9]" Type="Str">{40D2CABE-7850-43F9-9602-7E9C981A3E1B}</Property>
				<Property Name="Exe_actXinfo_objCLSIDsCount" Type="Int">14</Property>
				<Property Name="Exe_actXinfo_progIDPrefix" Type="Str">Main</Property>
				<Property Name="Exe_actXServerName" Type="Str">Main</Property>
				<Property Name="Exe_actXServerNameGUID" Type="Str">{3472735E-F78F-4197-AD4C-F683060FF3C7}</Property>
				<Property Name="Source[0].itemID" Type="Str">{8C0022A0-9F74-4ADD-B5E9-2ECEE57E0826}</Property>
				<Property Name="Source[0].type" Type="Str">Container</Property>
				<Property Name="Source[1].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[1].itemID" Type="Ref">/My Computer/Main.vi</Property>
				<Property Name="Source[1].sourceInclusion" Type="Str">TopLevel</Property>
				<Property Name="Source[1].type" Type="Str">VI</Property>
				<Property Name="Source[2].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[2].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[2].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[2].destinationIndex" Type="Int">3</Property>
				<Property Name="Source[2].itemID" Type="Ref">/My Computer/SubVis</Property>
				<Property Name="Source[2].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[2].type" Type="Str">Container</Property>
				<Property Name="Source[3].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[3].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[3].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[3].destinationIndex" Type="Int">4</Property>
				<Property Name="Source[3].itemID" Type="Ref">/My Computer/TypeDef</Property>
				<Property Name="Source[3].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[3].type" Type="Str">Container</Property>
				<Property Name="Source[4].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[4].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[4].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[4].destinationIndex" Type="Int">2</Property>
				<Property Name="Source[4].itemID" Type="Ref">/My Computer/Config</Property>
				<Property Name="Source[4].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[4].type" Type="Str">Container</Property>
				<Property Name="Source[5].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[5].itemID" Type="Ref"></Property>
				<Property Name="Source[5].type" Type="Str">VI</Property>
				<Property Name="Source[6].Container.applyInclusion" Type="Bool">true</Property>
				<Property Name="Source[6].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[6].destinationIndex" Type="Int">0</Property>
				<Property Name="Source[6].itemID" Type="Ref">/My Computer/Treiber</Property>
				<Property Name="Source[6].sourceInclusion" Type="Str">Include</Property>
				<Property Name="Source[6].type" Type="Str">Container</Property>
				<Property Name="Source[7].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[7].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[7].destinationIndex" Type="Int">5</Property>
				<Property Name="Source[7].itemID" Type="Ref">/My Computer/Treiber/HCS High Current</Property>
				<Property Name="Source[7].type" Type="Str">Container</Property>
				<Property Name="Source[8].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[8].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[8].destinationIndex" Type="Int">5</Property>
				<Property Name="Source[8].itemID" Type="Ref">/My Computer/Treiber/PI_E870</Property>
				<Property Name="Source[8].type" Type="Str">Container</Property>
				<Property Name="Source[9].Container.applyDestination" Type="Bool">true</Property>
				<Property Name="Source[9].Container.depDestIndex" Type="Int">0</Property>
				<Property Name="Source[9].destinationIndex" Type="Int">5</Property>
				<Property Name="Source[9].itemID" Type="Ref">/My Computer/Treiber/ximcLV x32</Property>
				<Property Name="Source[9].type" Type="Str">Container</Property>
				<Property Name="SourceCount" Type="Int">10</Property>
				<Property Name="TgtF_companyName" Type="Str">SEN - System Entwicklung Nordhausen GmbH</Property>
				<Property Name="TgtF_fileDescription" Type="Str">PC Steuerung</Property>
				<Property Name="TgtF_internalName" Type="Str">PC Steuerung</Property>
				<Property Name="TgtF_legalCopyright" Type="Str">Copyright © 2016 SEN - System Entwicklung Nordhausen GmbH</Property>
				<Property Name="TgtF_productName" Type="Str">PC Steuerung</Property>
				<Property Name="TgtF_targetfileGUID" Type="Str">{2EFA8A20-29A5-4306-8144-ECE263D0A38D}</Property>
				<Property Name="TgtF_targetfileName" Type="Str">Main.exe</Property>
			</Item>
		</Item>
	</Item>
</Project>
