﻿<?xml version='1.0' encoding='UTF-8'?>
<Library LVVersion="17008000">
	<Property Name="NI.Lib.Icon" Type="Bin">&amp;Q#!!!!!!!)!"1!&amp;!!!-!%!!!@````]!!!!"!!%!!!(]!!!*Q(C=\&gt;7R=2MR%!81N=?"5X&lt;A91P&lt;!FNA#^M#5Y6M96NA"R[WM#WQ"&lt;9A0ZYR'E?G!WPM1$AN&gt;@S(!ZZQG&amp;0%VLZ'@)H8:_X\&lt;^P(^7@8H\4Y;"`NX\;8JZPUX@@MJXC]C.3I6K5S(F/^DHTE)R`ZS%@?]J;XP/5N&lt;XH*3V\SEJ?]Z#F0?=J4HP+5&lt;Y=]Z#%0/&gt;+9@%QU"BU$D-YI-4[':XC':XB]D?%:HO%:HO(2*9:H?):H?)&lt;(&lt;4%]QT-]QT-]BNIEMRVSHO%R@$20]T20]T30+;.Z'K".VA:OAW"%O^B/GK&gt;ZGM&gt;J.%`T.%`T.)`,U4T.UTT.UTROW6;F.]XDE0-9*IKH?)KH?)L(U&amp;%]R6-]R6-]JIPC+:[#+"/7Q2'CX&amp;1[F#`&amp;5TR_2@%54`%54`'YN$WBWF&lt;GI8E==J\E3:\E3:\E-51E4`)E4`)EDW%D?:)H?:)H?5Q6S:-]S:-A;6,42RIMX:A[J3"Z`'S\*&lt;?HV*MENS.C&lt;&gt;Z9GT,7:IOVC7*NDFA00&gt;&lt;$D0719CV_L%7.N6CR&amp;C(7(R=,(1M4;Z*9.T][RNXH46X62:X632X61?X6\H(L8_ZYP^`D&gt;LP&amp;^8K.S_53Z`-Z4K&gt;4()`(/"Q/M&gt;`P9\@&lt;P&lt;U'PDH?8AA`XUMPTP_EXOF`[8`Q&lt;IT0]?OYVOA(5/(_Z!!!!!!</Property>
	<Property Name="NI.Lib.SourceVersion" Type="Int">385908736</Property>
	<Property Name="NI.Lib.Version" Type="Str">1.0.0.0</Property>
	<Item Name="Abstract Messages for Caller" Type="Folder"/>
	<Item Name="Messages for this Actor" Type="Folder">
		<Item Name="AngleDeltaB Msg.lvclass" Type="LVClass" URL="../Controller Messages/AngleDeltaB Msg/AngleDeltaB Msg.lvclass"/>
		<Item Name="CoilTimeMS Msg.lvclass" Type="LVClass" URL="../Controller Messages/CoilTimeMS Msg/CoilTimeMS Msg.lvclass"/>
		<Item Name="CommandChangeAlignment Msg.lvclass" Type="LVClass" URL="../Controller Messages/CommandChangeAlignment Msg/CommandChangeAlignment Msg.lvclass"/>
		<Item Name="CommandStartExp Msg.lvclass" Type="LVClass" URL="../Controller Messages/CommandStartExp Msg/CommandStartExp Msg.lvclass"/>
		<Item Name="CommandStartExp2 Msg.lvclass" Type="LVClass" URL="../Controller Messages/CommandStartExp2 Msg/CommandStartExp2 Msg.lvclass"/>
		<Item Name="CommandStartExp3 Msg.lvclass" Type="LVClass" URL="../Controller Messages/CommandStartExp3 Msg/CommandStartExp3 Msg.lvclass"/>
		<Item Name="CommandStartPos Msg.lvclass" Type="LVClass" URL="../Controller Messages/CommandStartPos Msg/CommandStartPos Msg.lvclass"/>
		<Item Name="DeinitRotmotor Msg.lvclass" Type="LVClass" URL="../Controller Messages/DeinitRotmotor Msg/DeinitRotmotor Msg.lvclass"/>
		<Item Name="Device_init Msg.lvclass" Type="LVClass" URL="../Controller Messages/Device_init Msg/Device_init Msg.lvclass"/>
		<Item Name="DistanceE Msg.lvclass" Type="LVClass" URL="../Controller Messages/DistanceE Msg/DistanceE Msg.lvclass"/>
		<Item Name="FineadjustmentFmax Msg.lvclass" Type="LVClass" URL="../Controller Messages/FineadjustmentFmax Msg/FineadjustmentFmax Msg.lvclass"/>
		<Item Name="FreezeTorsionOff Msg.lvclass" Type="LVClass" URL="../Controller Messages/FreezeTorsionOff Msg/FreezeTorsionOff Msg.lvclass"/>
		<Item Name="FreezeTorsionOn Msg.lvclass" Type="LVClass" URL="../Controller Messages/FreezeTorsionOn Msg/FreezeTorsionOn Msg.lvclass"/>
		<Item Name="GetRotmotorPosition Msg.lvclass" Type="LVClass" URL="../Controller Messages/GetRotmotorPosition Msg/GetRotmotorPosition Msg.lvclass"/>
		<Item Name="LightB1Top Msg.lvclass" Type="LVClass" URL="../Controller Messages/LightB1Top Msg/LightB1Top Msg.lvclass"/>
		<Item Name="LightB2Bottom Msg.lvclass" Type="LVClass" URL="../Controller Messages/LightB2Bottom Msg/LightB2Bottom Msg.lvclass"/>
		<Item Name="Power Msg.lvclass" Type="LVClass" URL="../Controller Messages/Power Msg/Power Msg.lvclass"/>
		<Item Name="RaspiCom Msg.lvclass" Type="LVClass" URL="../Controller Messages/SendSendRaspiCom Msg/RaspiCom Msg.lvclass"/>
		<Item Name="SetAngleAdjustment Msg.lvclass" Type="LVClass" URL="../Controller Messages/SetAngleAdjustment Msg/SetAngleAdjustment Msg.lvclass"/>
		<Item Name="SetFineAdjustment Msg.lvclass" Type="LVClass" URL="../Controller Messages/SetFineAdjustment Msg/SetFineAdjustment Msg.lvclass"/>
		<Item Name="StopwatchPause Msg.lvclass" Type="LVClass" URL="../Controller Messages/StopwatchPause Msg/StopwatchPause Msg.lvclass"/>
		<Item Name="StopwatchRaspiPause Msg.lvclass" Type="LVClass" URL="../Controller Messages/StopwatchRaspiPause Msg/StopwatchRaspiPause Msg.lvclass"/>
		<Item Name="StopwatchReset Msg.lvclass" Type="LVClass" URL="../Controller Messages/StopwatchReset Msg/StopwatchReset Msg.lvclass"/>
		<Item Name="StopwatchStart Msg.lvclass" Type="LVClass" URL="../Controller Messages/StopwatchStart Msg/StopwatchStart Msg.lvclass"/>
		<Item Name="StopwatchTime Msg.lvclass" Type="LVClass" URL="../Controller Messages/StopwatchTime Msg/StopwatchTime Msg.lvclass"/>
		<Item Name="StudentEmStop Msg.lvclass" Type="LVClass" URL="../Controller Messages/StudentEmStop Msg/StudentEmStop Msg.lvclass"/>
		<Item Name="StudentTorsionOnOff Msg.lvclass" Type="LVClass" URL="../Controller Messages/StudentTorsionOnOff Msg/StudentTorsionOnOff Msg.lvclass"/>
		<Item Name="TeacherTorsionOnOff Msg.lvclass" Type="LVClass" URL="../Controller Messages/TeacherTorsionOnOff Msg/TeacherTorsionOnOff Msg.lvclass"/>
		<Item Name="TorsionDuration Msg.lvclass" Type="LVClass" URL="../Controller Messages/TorsionDuration Msg/TorsionDuration Msg.lvclass"/>
	</Item>
	<Item Name="Controller.lvclass" Type="LVClass" URL="../Controller/Controller.lvclass"/>
</Library>
